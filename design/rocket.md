# Rocket Design

- Students bring their own bottle
- All bottles need to be PET
- 2 litre bottle is best
- Common interface at neck so that each bottle can fit to launcher
- No bottle _modification_ should be necessary, but **additions** can be
