## 0.9.0 (2023-10-14)

### Feat

- **education-goals**: add SkillsBuilder steps to map to project aims
- **URD**: expand UR to allow students to _design_ parts for rocket

### Fix

- **CI/CD**: attempt to move generated HTML to correct directory to address issue #2
- **CI/CD**: change job name to pages
- **CI/CD**: change output directory for StrictDoc
- **CI/CD**: specify artifacts keyword for built files
- **.gitlab-ci.yml**: move strictdoc export to build stage
- **Python**: bump Python environment to 3.10.12 and pyyaml to 6.0
- **.gitlab-ci.yml**: add image to get python

## 0.8.0 (2023-09-16)

### Feat

- add educational goals

## 0.7.0 (2023-07-30)

### Feat

- **URD**: expand UR to allow students to _design_ parts for rocket
- start drafting user personas
- **UML**: add qualification state diagram
- **SRD**: add several requirements and link to user requirements
- add UML document

### Fix

- **SRD**: fix issue #1 by acknowledging invalid assumption
- **URD**: trim line endings

### Refactor

- remove invalid assumption re year size
- restructure documents
- **Changelog**: move Changelog

## 0.6.0 (2023-07-15)

### Refactor

- remove Doorstop files to move to StrictDoc

## 0.5.0 (2023-07-14)

### Feat

- **SRD**: add rationale to SRD-001
- **URD**: link each URD to SSUN
- **DEF**: add definitions document
- **CSTRT**: add constraints document
- **ASMP**: add assumptions document
- **SRD**: add structure to SRD
- **URD**: start to recreate URD using StrictDoc

## 0.4.0 (2023-07-11)

### Feat

- **SRD**: add launcher life SR

## 0.3.0 (2023-07-09)

### Feat

- **SRD**: begin writing SRD (in Markdown)
- **DEFINITIONS**: expand detail about teachers

## 0.2.0 (2023-07-08)

### Feat

- **DEFINITIONS**: add top-level definition document

### Refactor

- **URD**: refocus URD002 to be _suitable_ and link intended USER002 to it
- **LINKS**: reverse direction of links following more research
- **LINKS**: reverse link direction FROM user requirement TO user

## 0.1.0 (2023-07-06)

### Feat

- **URD**: create users and link to applicable user requirements
- **CONSTR**: draft constraint related to rocket range
- **URD**: add requirements to improve visible aspects
- add assumption and constraint documents

### Fix

- **SSUN**: add missing "a"

### Refactor

- **URD**: review constraints
- **URD**: review new users
- **URD**: review changes and clear suspicion
- rename reqs to single statement of user need

## 0.0.1 (2023-06-29)
