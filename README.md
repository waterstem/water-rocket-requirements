## Versioning

This project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and [Commitizen](https://commitizen-tools.github.io/commitizen) with [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) to manage its git history.

Since this is not a software project, the definitions have been interpreted as follows. 

- fix: corrects errors and typos that don't affect parent, child or sibling objects (correlates with PATCH version)
- feat: adds a new feature or object e.g. a new requirement or solution (correlates with a MINOR version)
- docs
- style
- refactor
- perf
- test
- build
- ci


Given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible API changes, i.e.
2. MINOR version when you add functionality in a backward compatible manner
3. PATCH version when you make backward compatible bug fixes
