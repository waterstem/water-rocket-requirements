# User Stories

## Student

## Teacher

## Owner Operator

- As the owner of the system, Paul is able to demonstrate jet propulsion in a way that engages the students.
- Paul can describe the system before, during and after safe pressurisation, and either launch the rocket or safely depressurise it as required.

## STEM Ambassador
