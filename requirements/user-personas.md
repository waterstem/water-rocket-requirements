# User Personas

## Lucy

Year 8 student

### Lucy is

Attending the STEM day with the rest of her class. Lucy is in year 8 at school and bright, but wouldn't say she was interested in engineering. She doesn't want to stand out and is unlikely to volunteer, preferring to stay with her group of friends.

### Primary Motviation

Lucy just wants to get back to her favourite lessons without being embarrassed. She might joke about what happened later with her friends.

### Current Frustrations

Lucy doesn't really know where she fits in as far as careers go. She knows that when she and her friends talk about fantasy careers, it's not realistic. And she knows she is clever, but she can't really show it without standing out.

### Needs

- A vision of how engineering applies to much more of the built world than she realised
- An understanding of how her strong skills could be used for a wide variety of different things

## Mr Smith

### Mr Smith is

### Primary Motivation

### Current Frustrations

### Needs

## Paul

### Role

Owner/operator

### Primary Motivation

Design, build and document a system that demonstrates maths and physics at schools in an exciting and imaginative way. Paul is also keen to develop his engineering skills throughout this project.
